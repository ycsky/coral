/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.modules.prekit.quartz.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import com.gemframework.common.annotation.SysLog;
import com.gemframework.common.utils.GemBeanUtils;
import com.gemframework.common.constant.GemModules;
import com.gemframework.controller.BaseController;
import com.gemframework.model.common.BaseResultData;
import com.gemframework.model.common.PageInfo;
import com.gemframework.model.common.validator.SaveValidator;
import com.gemframework.model.common.validator.UpdateValidator;
import com.gemframework.model.enums.ExceptionCode;
import com.gemframework.model.enums.OperateType;
import com.gemframework.modules.prekit.quartz.entity.Jobs;
import com.gemframework.modules.prekit.quartz.entity.JobsVo;
import com.gemframework.modules.prekit.quartz.entity.enums.JobType;
import com.gemframework.modules.prekit.quartz.entity.validator.JobTypeCronValidator;
import com.gemframework.modules.prekit.quartz.entity.validator.JobTypeSimpleValidator;
import com.gemframework.modules.prekit.quartz.service.JobsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Title: JobsController
 * @Date: 2020-06-20 19:23:45
 * @Version: v1.0
 * @Description: 控制器
 * @Author: gem
 * @Email: gemframe@163.com
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Slf4j
@RestController
@RequestMapping(GemModules.PreKit.PATH_PRE+"/quartz/jobs")
public class JobsController extends BaseController {

    private static final String moduleName = "定时任务";

    @Autowired
    private JobsService jobsService;

    /**
     * 获取列表分页
     * @return
     */
    @GetMapping("/page")
    @RequiresPermissions("jobs:page")
    public BaseResultData page(PageInfo pageInfo, JobsVo vo) {
        Set<String> set = new HashSet<>();
        set.add("job_name,ASC");
        pageInfo.setSort_order(set);
        QueryWrapper queryWrapper = makeQueryMaps(vo,pageInfo);
        Page page = jobsService.page(setOrderPage(pageInfo),queryWrapper);
        return BaseResultData.SUCCESS(page.getRecords(),page.getTotal());
    }
    /**
     * 获取列表
     * @return
     */
    @GetMapping("/list")
    @RequiresPermissions("jobs:list")
    public BaseResultData list(JobsVo vo) {
        QueryWrapper queryWrapper = makeQueryMaps(vo);
        List list = jobsService.list(queryWrapper);
        return BaseResultData.SUCCESS(list);
    }

    /**
     * 添加
     * @return
     */
    @SysLog(type = OperateType.ALTER,value = "保存"+moduleName)
    @PostMapping("/save")
    @RequiresPermissions("jobs:save")
    public BaseResultData save(@RequestBody JobsVo vo) {
        if(vo.getJobType() == JobType.SIMPLE.getCode()){
            GemValidate(vo, SaveValidator.class,JobTypeSimpleValidator.class);
        }
        if(vo.getJobType() == JobType.CRON.getCode()){
            GemValidate(vo, SaveValidator.class, JobTypeCronValidator.class);
        }

        Jobs entity = GemBeanUtils.copyProperties(vo, Jobs.class);
        if(!jobsService.save(entity)){
            return BaseResultData.ERROR(ExceptionCode.SAVE_OR_UPDATE_FAIL);
        }
        return BaseResultData.SUCCESS(entity);
    }


    /**
     * 删除 & 批量刪除
     * @return
     */
    @SysLog(type = OperateType.ALTER,value = "删除"+moduleName)
    @PostMapping("/delete")
    @RequiresPermissions("jobs:delete")
    public BaseResultData delete(Long id,String ids) {
        if(id!=null) jobsService.removeById(id);
        if(StringUtils.isNotBlank(ids)){
            List<Long> listIds = Arrays.asList(ids.split(",")).stream().map(s ->Long.parseLong(s.trim())).collect(Collectors.toList());
            if(listIds!=null && !listIds.isEmpty()){
                    jobsService.removeByIds(listIds);
            }
        }
        return BaseResultData.SUCCESS();
    }


    /**
     * 编辑
     * @return
     */
    @SysLog(type = OperateType.ALTER,value = "编辑"+moduleName)
    @PostMapping("/update")
    @RequiresPermissions("jobs:update")
    public BaseResultData update(@RequestBody JobsVo vo) {
        GemValidate(vo, UpdateValidator.class);
        if(!jobsService.modifyJobTime(vo)){
            return BaseResultData.ERROR(ExceptionCode.SAVE_OR_UPDATE_FAIL);
        }
        return BaseResultData.SUCCESS();
    }


    /**
     * 获取用户信息ById
     * @return
     */
    @SysLog(type = OperateType.NORMAL,value = "查看"+moduleName)
    @GetMapping("/info")
    @RequiresPermissions("jobs:info")
    public BaseResultData info(Long id) {
        Jobs info = jobsService.getById(id);
        return BaseResultData.SUCCESS(info);
    }



    @SysLog(type = OperateType.NORMAL,value = "执行"+moduleName)
    @GetMapping("/exec")
    @RequiresPermissions("jobs:exec")
    public BaseResultData exec(JobsVo jobsVo) {
        if(!jobsService.startJob(jobsVo)){
            return BaseResultData.ERROR(ExceptionCode.EXEC_JOB_FAIL);
        }
        return BaseResultData.SUCCESS();
    }


    @SysLog(type = OperateType.NORMAL,value = "暂停"+moduleName)
    @GetMapping("/stop")
    @RequiresPermissions("jobs:stop")
    public BaseResultData stop(JobsVo jobsVo) {
        if(!jobsService.stopJob(jobsVo)){
            return BaseResultData.ERROR(ExceptionCode.STOP_JOB_FAIL);
        }
        return BaseResultData.SUCCESS();
    }

}