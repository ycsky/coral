/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.modules.prekit.quartz.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.gemframework.modules.prekit.quartz.entity.Jobs;
import com.gemframework.modules.prekit.quartz.entity.JobsVo;

/**
 * @Title: JobsService
 * @Date: 2020-06-20 19:23:45
 * @Version: v1.0
 * @Description: 服务接口
 * @Author: gem
 * @Email: gemframe@163.com
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
public interface JobsService extends IService<Jobs> {

    /**
     * 启动任务
     */
    boolean startJob(JobsVo jobsVo);

    /**
     * 暂停任务
     */
    boolean stopJob(JobsVo jobsVo);

    /**
     * 修改任务执行时间
     */
    boolean modifyJobTime(JobsVo jobsVo);

    boolean initJobs();
}