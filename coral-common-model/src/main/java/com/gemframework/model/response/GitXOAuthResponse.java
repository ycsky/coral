/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.model.response;

import lombok.Data;

@Data
public class GitXOAuthResponse {
    //第三方返回数据
    private Integer id;
    //昵称
    private String name;
    //头像地址
    private String avatarUrl;
    //登录帐号
    private String login;
    //个人介绍
    private String bio;
    //个人博客
    private String blog;
    //主页地址
    private String htmlUrl;
    //创建时间
    private String createdAt;
    //更新时间
    private String updatedAt;
}