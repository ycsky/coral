/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.model.entity.bo.sigar;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @Title: ServerDiskInfo
 * @Package: com.gemframework.model.entity.bo.sigar
 * @Date: 2020-07-30 22:34:19
 * @Version: v1.0
 * @Description: 服务器磁盘信息
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Data
@Builder
public class ServerDiskInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    //分区的盘符名称0
    private Integer devId;
    //盘符名称:    C:\
    private String devName;
    //盘符路径:    C:\
    private String dirName;
    //盘符标志:    0
    private Long devFlag;
    //盘符类型:    NTFS
    private String sysType;
    //盘符类型名:    local
    private String devType;
    //盘符文件系统类型:    2
    private Integer fileType;
    //C:\总大小:    117218240KB
    private double total;
    //C:\剩余大小:    62509844KB
    private double free;
    //C:\可用大小:    62509844KB
    private double avail;
    //C:\已经使用量:    54708396KB
    private double used;
    //C:\资源的利用率:    47.0%
    private double usePercent;
    //C:\读出：    499427
    private Long reads;
    //C:\写入：    750460
    private Long writes;
}
