/**
 * 严肃声明：
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.model.entity.bo.sigar;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gemframework.model.enums.OssStorageType;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @Title: ServerCPUInfo
 * @Package: com.gemframework.model.entity.bo
 * @Date: 2020-07-30 22:26:19
 * @Version: v1.0
 * @Description: 服务器CPU信息
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Data
@Builder
public class ServerCPUInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	//	CPU索引
	private Integer index;
	//	CPU的总量MHz:    2494
	private Integer mhz;
	//	CPU生产商:    Intel
	private String vendor;
	//	CPU类别:    Core(TM) i5-4200M CPU @ 2.50GHz
	private String model;
	//	CPU缓存数量:    -1
	private Long cacheSize;
	//	CPU当前等待率:    0.0%
	private double wait;
	//	CPU当前错误率:    0.0%
	private double error;
	//	CPU当前空闲率:    84.7%
	private double free;
	//	CPU总的使用率:    15.2%
	private double used;
	//	CPU用户使用率:    9.0%
	private double userUsed;
	//	CPU系统使用率:    6.1%
	private double sysUsed;
}
