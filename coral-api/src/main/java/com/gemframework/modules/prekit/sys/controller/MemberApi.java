/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.modules.prekit.sys.controller;
import com.gemframework.common.annotation.ApiLog;
import com.gemframework.common.annotation.ApiSign;
import com.gemframework.common.annotation.ApiToken;
import com.gemframework.common.utils.GemValidateUtils;
import com.gemframework.model.common.BaseResultData;
import com.gemframework.modules.prekit.sys.entity.request.MemberSetPasswordRequest;
import com.gemframework.service.MemberService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
public class MemberApi {

	@Resource
	private MemberService memberService;


	@ApiLog(name = "修改密码")
	@PostMapping("setPassword")
	@ApiSign
	@ApiToken
	public BaseResultData setPassword(MemberSetPasswordRequest request) {
		//参数校验器
		GemValidateUtils.GemValidate(request);
		memberService.setPassword(request.getMemberId(),request.getPassword());
		return BaseResultData.SUCCESS();
	}

}
