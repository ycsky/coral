/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.common.config.mybatis;

import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.config.GlobalConfig;
import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.gemframework.common.utils.GemConfigUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Title: MyBatisPlusConfig
 * @Package: com.gemframework.config
 * @Date: 2020-03-09 14:42:33
 * @Version: v1.0
 * @Description: MyBatisPlusConfig配置
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */
@Slf4j
@EnableTransactionManagement //开启事务
@Configuration
@MapperScan(basePackages = {"com.gemframework.mapper",
        "com.gemframework.modules.*.*.mapper",
        "com.gemframework.generator.mapper"})
public class MyBatisPlusConfig extends MybatisConfiguration {

    private static String scanPackages = "";
    static {
        scanPackages = (String) GemConfigUtils.getProperties("gem.mybatis.scan-packages");
        if(scanPackages == null || StringUtils.isBlank(scanPackages)){
            scanPackages = (String) GemConfigUtils.getYmlConfig("gem.mybatis.scan-packages");
        }
    }

    /**
     * @description: 配置分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        log.debug("注册分页插件");
        return new PaginationInterceptor();
    }

    @Bean
    public GlobalConfig globalConfig() {
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setMetaObjectHandler(new MetaDataHandler());
        return globalConfig;
    }

    /**
     * 配置mapper文件多个扫描路径
     * @Auth 769990999@qq.com
     * @return
     * @Copyright: Copyright (c) 2020 gemframework.com
     */
    public Resource[] resolveMapperLocations() {
        ResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();
        List<String> mapperLocations = new ArrayList<>();
        //默认扫包配置
        mapperLocations.add("classpath*:mapper/*.xml");
        mapperLocations.add("classpath*:common_mapper/*.xml");
        mapperLocations.add("classpath*:common_mapper/**/*.xml");
        //动态读取配置文件
        if(StringUtils.isNotBlank(scanPackages)){
            String[] scanPackageArr = scanPackages.split(",");
            if(scanPackageArr.length>0){
                for(String scanPack:scanPackageArr){
                    mapperLocations.add(scanPack);
                }
            }
        }
        List<Resource> resources = new ArrayList();
        if (mapperLocations != null) {
            for (String mapperLocation : mapperLocations) {
                try {
                    Resource[] mappers = resourceResolver.getResources(mapperLocation);
                    resources.addAll(Arrays.asList(mappers));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return resources.toArray(new Resource[resources.size()]);
    }
}