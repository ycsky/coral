/**
 * 开源版本请务必保留此注释头信息，若删除捷码开源〔GEMOS〕官方保留所有法律责任追究！
 * 本软件受国家版权局知识产权以及国家计算机软件著作权保护（登记号：2018SR503328）
 * 不得恶意分享产品源代码、二次转售等，违者必究。
 * Copyright (c) 2020 gemframework all rights reserved.
 * http://www.gemframework.com
 * 版权所有，侵权必究！
 */
package com.gemframework.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gemframework.model.entity.po.Datasource;


/**
 * @Title: DatasourceService
 * @Package: com.gemframework.service.datasource
 * @Date: 2020-06-29 21:27:40
 * @Version: v1.0
 * @Description: Datasource服务
 * @Author: nine QQ 769990999
 * @Copyright: Copyright (c) 2020 wanyong
 * @Company: www.gemframework.com
 */

public interface DatasourceService extends IService<Datasource> {

    /**
     * 动态切换数据源
     * @param dsName
     * @return
     * @throws Exception
     */
    boolean change(String dsName);

    /**
     * 测试连接
     * @param datasource
     * @return
     */
    boolean testConnection(Datasource datasource);

    /**
     * 更新数据源信息
     * @param datasource
     * @return
     */
    boolean save(Datasource datasource);

    /**
     * 删除数据源
     * @param id
     * @param ids
     * @return
     */
    boolean delete(Long id, String ids);

    /**
     * 更新数据源信息
     * @param datasource
     * @return
     */
    boolean update(Datasource datasource);

    /**
     * 数据源是否存在
     * @param entity
     * @return
     */
    boolean exits(Datasource entity);

}